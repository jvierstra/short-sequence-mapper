########################################
# definitions - change these!
########################################

human_peaks = /path/to/human/peaks
mouse_peaks = /path/to/mouse/peaks

human_over_chain = /path/to/hg19ToMm9.over.chain.gz
mouse_over_chain = /path/to/mm9ToHg19.over.chain.gz

build_dir = build

########################################
# intermediate files
########################################

human_master_peaks = $(build_dir)/master-peaks/master-peaks.human.bed
mouse_master_peaks = $(build_dir)/master-peaks/master-peaks.mouse.bed

human_master_peaks_chroms = $(addprefix $(build_dir)/master-peaks/human/, $(human_chroms))
mouse_master_peaks_chroms = $(addprefix $(build_dir)/master-peaks/mouse/, $(mouse_chroms))

master_peaks = $(human_master_peaks) $(mouse_master_peaks)

human_master_peaks_mapped = $(build_dir)/master-peaks-mapped/master-peaks.human.mapped-mm9.bed
mouse_master_peaks_mapped = $(build_dir)/master-peaks-mapped/master-peaks.mouse.mapped-hg19.bed

human_master_peaks_chroms_mapped = $(addprefix $(build_dir)/master-peaks-mapped/human/, $(human_chroms))
mouse_master_peaks_chroms_mapped = $(addprefix $(build_dir)/master-peaks-mapped/mouse/, $(mouse_chroms))

master_peaks_mapped = $(human_master_peaks_mapped) $(mouse_master_peaks_mapped)
master_peaks_mapped_blocks = $(master_peaks_mapped:%.bed=%.blocks.bed)
master_peaks_mapped_blocks_gff = $(master_peaks_mapped:%.bed=%.blocks.gff)

human_master_peaks_mapped_peaks_blocks = $(build_dir)/master-peaks-mapped/master-peaks.human.mapped-mm9.peaks-blocks.bed
human_master_peaks_mapped_blocks_peaks = $(build_dir)/master-peaks-mapped/master-peaks.human.mapped-mm9.blocks-peaks.bed
human_mouse_peaks = $(build_dir)/master-peaks-mapped/master-peaks.human.mapped-mm9.mouse.peaks.txt

mouse_master_peaks_mapped_peaks_blocks = $(build_dir)/master-peaks-mapped/master-peaks.mouse.mapped-hg19.peaks-blocks.bed
mouse_master_peaks_mapped_blocks_peaks = $(build_dir)/master-peaks-mapped/master-peaks.mouse.mapped-hg19.blocks-peaks.bed
mouse_human_peaks = $(build_dir)/master-peaks-mapped/master-peaks.mouse.mapped-hg19.human.peaks.txt

reciprocal_peaks = $(build_dir)/master-peaks-mapped/reciprocal-peaks.txt
reciprocal_peaks_coords = $(build_dir)/master-peaks-mapped/reciprocal-peaks.coords.txt

reciprocal_peaks_human = $(build_dir)/master-peaks-mapped/reciprocal-peaks.human.bed
reciprocal_peaks_mouse = $(build_dir)/master-peaks-mapped/reciprocal-peaks.mouse.bed

########################################
# target definitions
########################################

all: $(reciprocal_peaks_human) $(reciprocal_peaks_mouse)

########################################
# reformat peaks
########################################

$(human_master_peaks):
	mkdir -p $(shell dirname $@)/human;\
	src/util/reformat-split-peaks.sh\
		$(human_peaks)\
		$(build_dir)/master-peaks/human\
	> $@

$(human_master_peaks_chroms): $(human_master_peaks)

$(mouse_master_peaks):
	mkdir -p $(shell dirname $@)/mouse;\
	src/util/reformat-split-peaks.sh\
		$(mouse_peaks)\
		$(build_dir)/master-peaks/mouse\
	> $@

$(mouse_master_peaks_chroms): $(mouse_master_peaks)


########################################
# mapping
########################################

$(human_master_peaks_chroms_mapped): $(build_dir)/master-peaks-mapped/human/%: $(build_dir)/master-peaks/human/%
	mkdir -p $(shell dirname $@);\
	bash/bn-mapper-wrapper.sh\
		$(human_over_chain)\
		$< $@

$(human_master_peaks_mapped): $(human_master_peaks_chroms_mapped)
	cat $^ > $@

$(mouse_master_peaks_chroms_mapped): $(build_dir)/master-peaks-mapped/mouse/%: $(build_dir)/master-peaks/mouse/% 
	mkdir -p $(shell dirname $@);\
	bash/bn-mapper-wrapper.sh\
		$(mouse_over_chain)\
		$< $@

$(mouse_master_peaks_mapped): $(mouse_master_peaks_chroms_mapped)
	cat $^ > $@

########################################
# post-process mapping
########################################

$(master_peaks_mapped_blocks): $(build_dir)/master-peaks-mapped/master-peaks%.blocks.bed: $(build_dir)/master-peaks-mapped/master-peaks%.bed
	cat $< | python/convert-to-bed.py | bbms - > $@

$(master_peaks_mapped_blocks_gff): $(build_dir)/master-peaks-mapped/master-peaks%.blocks.gff: $(build_dir)/master-peaks-mapped/master-peaks%.bed
	cat $< | python/convert-to-gff.py > $@

########################################
# find the overlaping peaks
########################################

$(human_mouse_peaks): $(build_dir)/master-peaks-mapped/master-peaks.human.mapped-mm9.blocks.bed
	bash/reformat-mapped-peaks.sh\
		$(human_master_peaks)\
		$(mouse_master_peaks)\
		$(build_dir)/master-peaks-mapped/master-peaks.human.mapped-mm9.blocks.bed\
		$(human_master_peaks_mapped_peaks_blocks)\
		$(human_master_peaks_mapped_blocks_peaks)\
		$(human_mouse_peaks)

$(mouse_human_peaks): $(build_dir)/master-peaks-mapped/master-peaks.mouse.mapped-hg19.blocks.bed
	bash/reformat-mapped-peaks.sh\
		$(mouse_master_peaks)\
		$(human_master_peaks)\
		$(build_dir)/master-peaks-mapped/master-peaks.mouse.mapped-hg19.blocks.bed\
		$(mouse_master_peaks_mapped_peaks_blocks)\
		$(mouse_master_peaks_mapped_blocks_peaks)\
		$(mouse_human_peaks)

########################################
# find the reciprocal peaks
########################################

$(reciprocal_peaks): $(human_mouse_peaks) $(mouse_human_peaks)
	python/parse-reciprocal-peaks.py $(human_mouse_peaks) $(mouse_human_peaks) > $@

$(reciprocal_peaks_coords): $(reciprocal_peaks)
	python/replace-with-coords.py $(human_master_peaks) $(mouse_master_peaks) < $< > $@

$(reciprocal_peaks_human): $(reciprocal_peaks_coords)
	cut -f1-4 $< | sort-bed - | uniq > $@

$(reciprocal_peaks_mouse): $(reciprocal_peaks_coords)
	cut -f5-8 $< | sort-bed - | uniq > $@
