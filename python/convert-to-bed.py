#!/bin/env python

"""
2012-11-26: jdv

Converts the BED12 output of bnMapper (bx-python) into a bed file.
Outputs one line for each alignment block, ID field from the orginal species block.

Note: Each input peak to bnMapper could produce many alignment blocks depending
on the parameters.

"""

import sys

for line in sys.stdin:
	
	vals = line.strip().split()

	(chr, start, end, id) = vals[:4]
	
	start = int(start)
	end = int(end)

	block_lens = [ int(x) for x in vals[10].split(',') ]
	block_starts = [ int(x) for x in vals[11].split(',') ]

	block_starts = [ (x + start) for x in block_starts ]
	blocks = [ (x, x + y) for (x, y) in zip(block_starts, block_lens) ]
	
	for block_start, block_end in blocks:

		fields = [ chr, block_start, block_end, id ]
		print '\t'.join( [ str(x) for x in fields ] )


