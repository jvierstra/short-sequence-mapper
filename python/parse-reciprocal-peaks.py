#!/bin/env python

"""
2012-11-26: jdv

After mapping the respective species peaks into the corresponding 
genomes, overlap the results to generate *reciprocal peaks*

Outputs a list of peak connections, by peak id (not coordinate).

"""

import sys

human_to_mouse = {} # key = human peak; value = set of mouse peaks overlapping the mapping region
mouse_to_human = {}

#print "+ Reading human -> mouse file ..."

for line in open(sys.argv[1]):
	(human_peak, mouse_peaks) = line.strip().split('\t')
	
	if not human_peak in human_to_mouse:
		human_to_mouse[human_peak] = set()
	
	human_to_mouse[human_peak] |= set( mouse_peaks.split(';') )

#print "+ Reading mouse -> human file ..."

for line in open(sys.argv[2]):
	(mouse_peak, human_peaks) = line.strip().split('\t')
	
	if not mouse_peak in mouse_to_human:
		mouse_to_human[mouse_peak] = set()
	
	mouse_to_human[mouse_peak] |= set( human_peaks.split(';') )

#print "- No. human -> mouse: %d" % len(human_to_mouse)
#print "- No. mouse -> human: %d" % len(mouse_to_human)

def write(e):
    for x in e:
        (a, b) = x.split('|')
        print '\t'.join( [a, b] )

def make_keys(a_peak, b_peaks_set, rev = False):
    if rev:
        return [ "%s|%s" % (b_peak, a_peak) for b_peak in b_peaks_set ]
    else:
        return [ "%s|%s" % (a_peak, b_peak) for b_peak in b_peaks_set ]


def resolve(a, b, rev = False):
    
    # return a key
    res = set()
    
    for a_peak, b_peaks in a.items():
        
        b_peaks_set = set()
        
        for b_peak in b_peaks:
			
		try:
				
			recip_a_peaks_set = b[b_peak]
			
			if a_peak in recip_a_peaks_set:
				b_peaks_set.add(b_peak)
						
		except:
			continue

        res |= set( make_keys(a_peak, b_peaks_set, rev) )
    
    return res

hm = resolve(human_to_mouse, mouse_to_human)
mh = resolve(mouse_to_human, human_to_mouse, rev = True)

# take all the truely reciprocal peaks 
write( hm & mh )
