#!/bin/env python

"""
2012-11-26: jdv

Converts the BED12 output of bnMapper (bx-python) into a GFF file 
for visualizations of the mapper in the UCSC browser.

"""

import sys

i = 1
for line in sys.stdin:
	
	vals = line.strip().split()

	(chr, start, end) = vals[:3]
	start = int(start)
	end = int(end)

	block_lens = [ int(x) for x in vals[10].split(',') ]
	block_starts = [ int(x) for x in vals[11].split(',') ]

	block_starts = [ (x + start) for x in block_starts ]
	blocks = [ (x, x + y) for (x, y) in zip(block_starts, block_lens) ]
	
	score = "%0.4f" % ( float( sum(block_lens) ) / 150. )
	strand = '.'
	frame = '.'
	group = "id-%d" % i

	for block_start, block_end in blocks:

		fields = [ chr, 'bn-map', 'dhs', block_start, block_end, score, strand, frame, group ]
		print '\t'.join( [ str(x) for x in fields ] )

	i += 1

