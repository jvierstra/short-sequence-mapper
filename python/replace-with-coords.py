#!/bin/env python

import sys

a = {}
b = {}

for line in open(sys.argv[1]):
    (chr, start, end, id) = line.strip().split('\t')
    a[id] = '\t'.join( [chr, start, end] )

for line in open(sys.argv[2]):
    (chr, start, end, id) = line.strip().split('\t')
    b[id] =  '\t'.join( [chr, start, end] )

n = 1
for line in sys.stdin:
    (aid, bid) = line.strip().split('\t')
    print '\t'.join( [a[aid], aid, b[bid], bid, str(n)] )
    n += 1
