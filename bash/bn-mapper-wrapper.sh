#!/bin/bash

chain=$1
peaks_file=$2
outfile=$3

# check to see if we are running on the SGE cluster or not
# and set the temp dir accordingly

if [ -z ${TMPDIR} ]; then
	tmpdir=/tmp/`whoami`/$$
	mkdir -p ${tmpdir}
else
	tmpdir=\${TMPDIR}
fi


## uncompress chain
zcat ${chain} > ${tmpdir}/chain

## reformat input 
cat ${peaks_file} | awk -v OFS="\t" '{ print $1, $2, $3, $1":"$2"-"$3; }' > ${tmpdir}/in

## map (note the parameters)
bnMapper.py -fBED12 --gap 20 --threshold 0.1 ${tmpdir}/in ${tmpdir}/chain -o ${outfile}

## cleanup
rm -fr ${tmpdir}