#!/bin/bash

#
# Converts the processed output from the mapping scripts
# into putative sites of overlap.
#
# Inputs are three files:
#	(1) species A peaks
#	(2) species B peaks
#	(3) species A peaks mapped into species B 
#		(after processing with "convert_to_peak.py")
#
# Outputs are three files:
#	(1) BED file: chrA startA endA chrB:startB-endB idA
#	(2) BED file: chrB startB endB idA
#	(3) txt file: idA idB.1;idB.2;...
#

from_peaks_file=$1
to_peaks_file=$2

mapped_blocks_file=$3

mapped_peaks_blocks_file=$4
mapped_blocks_peaks_file=$5

shared_peaks_file=$6

cat ${mapped_blocks_file}\
| awk -v OFS="\t" '{ gsub(/-|:/, "\t", $4); print $4, $1":"$2"-"$3; }'\
| bbms - | bedmap --delim "\t" --echo --echo-map-id - ${from_peaks_file}\
| tee ${mapped_peaks_blocks_file}\
| awk -v OFS="\t" '{ gsub(/:|-/, "\t", $4); print $4, $5; }'\
| bbms - | tee ${mapped_blocks_peaks_file}\
| bedmap --ec --delim "\t" --indicator --echo --echo-map-id - ${to_peaks_file}\
| awk -v OFS="\t" '$1 == 1 { print $5, $6; }'\
> ${shared_peaks_file}