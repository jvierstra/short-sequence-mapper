#!/bin/bash

# jdv 2012-11-26
#
# Reprocesses and splits a peaks file
#

peaks_file=$1
outdir=$2

cat ${peaks_file}\
| awk -v OFS="\t" -v outdir=${outdir} '{\
	print $1, $2, $3, "id-"NR;\
	print $1, $2, $3, "id-"NR > outdir"/"$1;\
}'